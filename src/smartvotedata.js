
import {default as rp} from 'request-promise';

const BASE_URL = 'https://api.smartvote.ch/2.0';

//to get a new one ;-)
//https://api.smartvote.ch/auth/token
var AUTH_TOKEN = '9b92e1e6-3f68-4c22-8a84-ec7a6837ea10';

var request = rp.defaults({
  json: true,
  headers: {'x-auth-token': AUTH_TOKEN},
  baseUrl: BASE_URL
});

function getElection(id) {
  return request.get('/elections/' + id);
}

function getConstituencies(election) {
  return request.get('/elections/' + election.id + '/constituencies')
    .then(function(constituencies) {
      return constituencies.map(function(c) { c.electionId = election.id; return c; });
    });
}

function getParties(electionId, constituencyId) {
  if (!constituencyId) {
    return request.get('/elections/' + electionId + '/parties?rootParties=true');
  }
  return request.get('/elections/' + electionId + '/parties?constituencyId=' + constituencyId);
}

function getCandidates(electionId, params) {
  return request({
    url: '/elections/' + electionId + '/candidates',
    qs: params
  }).then(function(candidates) {
    return candidates.map(function(candidate) { candidate.partyUrl = candidate._links.party; return candidate;});
  });
}

function getParty(candidate) {
  return request(candidate.partyUrl);
}

export {
  getElection,
  getConstituencies,
  getParties,
  getCandidates,
  getParty
};
