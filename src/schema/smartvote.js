
import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList
} from 'graphql';

import { getElection, getConstituencies, getParties, getCandidates, getParty } from '../smartvotedata';

var candidateType, partyType, constituencyType, electionType, queryType;

/**
 * Candidate
 */
candidateType = new GraphQLObjectType({
  name: 'Candidate',
  description: 'A candidate of an election',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the candidate.',
    },
    firstname: {
      type: GraphQLString,
      description: 'The first name of the candidate.',
    },
    lastname: {
      type: GraphQLString,
      description: 'The last name of the candidate.',
    },
    party: {
      type: partyType,
      description: 'The party of the candidate.',
      resolve: candidate => getParty(candidate)
    }
  })
});


/**
 * Party
 */
partyType = new GraphQLObjectType({
  name: 'Party',
  description: 'A party of an election',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the party.',
    },
    name: {
      type: GraphQLString,
      description: 'The name of the party.',
    },
    abbreviation: {
      type: GraphQLString,
      description: 'The abbreviation of the party.',
    }
  })
});


/**
 * Constituency
 */
constituencyType = new GraphQLObjectType({
  name: 'Constituency',
  description: 'A constituency (Wahlkreis) of an election',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the constituency.',
    },
    electionId: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the election.',
    },
    name: {
      type: GraphQLString,
      description: 'The name of the constituency.',
    },
    nofSeats: {
      type: GraphQLInt,
      description: 'The number of seats this constituency has.',
    },
    parties: {
      type: new GraphQLList(partyType),
      description: 'The parties of the constituency, or an empty list if there are none',
      resolve: constituency => getParties(constituency.electionId, constituency.id)
    }
  })
});

/**
 * Election
 */
electionType = new GraphQLObjectType({
  name: 'Election',
  description: 'An election',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the election.',
    },
    name: {
      type: GraphQLString,
      description: 'The name of the election.',
    },
    constituencies: {
      type: new GraphQLList(constituencyType),
      description: 'The constituencies of the election, or an empty list if there are none',
      resolve: election => getConstituencies(election),
    },
    parties: {
      type: new GraphQLList(partyType),
      description: 'The parties of the election, or an empty list if there are none',
      args: {
        constituencyId: {
          description: 'id of a constituency. if omitted return root parties',
          type: GraphQLString
        }
      },
      resolve: (election, {constituencyId}) => getParties(election.id, constituencyId)
    },
    candidates: {
      type: new GraphQLList(candidateType),
      description: 'The candidates of the election, or an empty list if there are none',
      args: {
        constituencyId: {
          description: 'id of a constituency. if omitted return candidates of all constituencies',
          type: GraphQLString
        },
        partyId: {
          description: 'id of a party. if omitted return candidates of all parties',
          type: GraphQLString
        },
      },
      resolve: (election, args) => getCandidates(election.id, args)
    }
  })
});

// Root Query Type
queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    election: {
      type: electionType,
      args: {
        id: {
          description: 'id of an election (use "98" ;-))',
          type: new GraphQLNonNull(GraphQLString)
        }
      },
      resolve: (root, { id }) => getElection(id),
    }
  })
});

export {
  queryType
};
