
import {
  GraphQLSchema,
} from 'graphql';

import { queryType } from './smartvote';

const schema = new GraphQLSchema({
  query: queryType
  //mutation: mutationType 
});

export default schema;
