
import express from 'express';
import graphqlHTTP from 'express-graphql';

import schema from './schema';

express()
  .use('/graphql', graphqlHTTP({ schema: schema, pretty: true, graphiql: true }))
  .listen(3000, function() {
    console.log('listening on port 3000');
  });
