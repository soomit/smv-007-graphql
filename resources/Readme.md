# Iframe
What elections to include + some visual infos (ex. css)

https://api.smartvote.ch/2.0/iframes/ch15_srf?lang=de

# Election
https://api.smartvote.ch/2.0/elections/98

## Statistics
Resolved automatically ("_links" attribute)

# Constituencies (Wahlkreise)
https://api.smartvote.ch/2.0/elections/98/constituencies?lang=de

# Parties
https://api.smartvote.ch/2.0/elections/98/parties?lang=de&rootParties=true

# Candidates
https://api.smartvote.ch/2.0/elections/98/candidates?constituencyId=19400000001&lang=de&partyId=19400000032





