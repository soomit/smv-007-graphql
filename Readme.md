# Sample GraphQL server using smartvote.ch
Have a look at package.json

## Getting started (dev)
```
$ npm install
$ npm run-script watch
```

In a separate shell (no autoreload support ;-)
```
$ node dist/server.js
```
